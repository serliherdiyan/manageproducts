package com.serli.manageproducts;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;

@SpringBootApplication
@EntityScan(basePackages = "com.serli.manageproducts.entity")
public class ManageproductsApplication {
    public static void main(String[] args) {
        SpringApplication.run(ManageproductsApplication.class, args);
    }
}

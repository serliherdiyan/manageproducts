package com.serli.manageproducts.config;

import org.springframework.context.annotation.Configuration;

@Configuration
public class AppConfig {
    // Tidak perlu mendefinisikan bean ProductService di sini
    // Spring akan secara otomatis membuat dan menyuntikkan bean ProductService
}

package com.serli.manageproducts.repository;

import com.serli.manageproducts.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {
    @Query(value = "SELECT * FROM manageproduct WHERE price > :price", nativeQuery = true)
    List<Product> findProductsAbovePrice(@Param("price") double price);
}

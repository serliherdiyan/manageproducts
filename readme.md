**ManageProducts Application**

Ini adalah contoh aplikasi Spring Boot sederhana yang memungkinkan Anda untuk menambahkan, mengambil, dan mencari produk berdasarkan harga. Aplikasi ini menggunakan Spring Boot, Spring Data JPA, dan H2 Database.

**Cara Menjalankan Aplikasi:**

1. Pastikan Anda memiliki Java JDK 11 terinstal di komputer Anda.

2. Unduh atau salin kode dari repositori ini.

3. Buka terminal atau command prompt dan navigasi ke direktori aplikasi.

4. Jalankan perintah berikut untuk menjalankan aplikasi:

   ```
   ./mvnw spring-boot:run
   ```

   Jika Anda menggunakan Windows, Anda dapat menjalankan perintah ini:

   ```
   mvnw spring-boot:run
   ```

5. Aplikasi akan mulai berjalan dan dapat diakses di `http://localhost:8080`.

**Endpoint yang Tersedia:**

- `GET /products/above-price/{price}`: Mengambil daftar produk dengan harga di atas nilai tertentu.
  
  Contoh: `GET http://localhost:8080/products/above-price/50.0`

- `POST /products/add`: Menambahkan produk baru.

  Contoh Payload:
  ```json
  {
    "name": "Product A",
    "price": 60.0
  }
  ```

  Contoh: `POST http://localhost:8080/products/add`

- `GET /products/all`: Mengambil semua produk yang tersedia.

  Contoh: `GET http://localhost:8080/products/all`

**Struktur Proyek:**

- `src/main/java/com/serli/manageproducts`: Direktori utama untuk kode aplikasi.
  - `config`: Direktori yang berisi konfigurasi Spring.
  - `controller`: Direktori yang berisi controller REST API.
  - `entity`: Direktori yang berisi definisi entitas produk.
  - `repository`: Direktori yang berisi definisi repositori JPA.
  - `service`: Direktori yang berisi logika bisnis dan layanan.
  - `ManageproductsApplication.java`: Kode untuk menjalankan aplikasi.

- `src/main/resources`: Direktori untuk sumber daya seperti file konfigurasi.
  - `application.properties`: Konfigurasi aplikasi, termasuk konfigurasi H2 Database.

**Catatan:**

Pastikan Anda telah menginstal Maven Wrapper (`mvnw` atau `mvnw.cmd`) di direktori proyek Anda. Jika tidak, Anda dapat mengunduhnya dengan menjalankan perintah berikut:

```
mvn wrapper:wrapper
```

Dengan menjalankan aplikasi ini, Anda akan memiliki aplikasi manajemen produk sederhana yang memungkinkan Anda menambahkan, mengambil, dan mencari produk berdasarkan harga. Jangan ragu untuk menyesuaikan dan memodifikasi aplikasi ini sesuai dengan kebutuhan Anda. Jika Anda memiliki pertanyaan lebih lanjut atau kesulitan, jangan ragu untuk bertanya kepada komunitas atau pengembang lainnya. Selamat menjelajahi dan belajar dengan aplikasi ini!